package graph

//go:generate go run github.com/99designs/gqlgen generate

import (
	"gitlab.com/jimboylabs/go-commerce/app"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	App *app.Application
}
