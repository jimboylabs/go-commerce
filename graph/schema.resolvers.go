package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.
// Code generated by github.com/99designs/gqlgen version v0.17.31

import (
	"context"
	"fmt"
	"strconv"

	"github.com/spf13/cast"
	"gitlab.com/jimboylabs/go-commerce/calculator"
	"gitlab.com/jimboylabs/go-commerce/graph/model"
	commerce "gitlab.com/jimboylabs/go-commerce/proto"
)

// CheckoutCreate is the resolver for the checkoutCreate field.
func (r *mutationResolver) CheckoutCreate(ctx context.Context, input *model.CheckoutCreateInput) (*model.CheckoutCreate, error) {
	id, err := r.App.DB.CheckoutCreate(defaultChannelID, input.Email)
	if err != nil {
		return nil, err
	}

	commerceLines := make([]*commerce.Line, 0)
	lines := make([]*model.LineItem, 0)

	checkout := &model.Checkout{ID: cast.ToString(id)}

	if len(input.Lines) > 0 {
		for _, l := range input.Lines {
			//  get product variant data from database

			price, err := r.App.DB.ProductVariantPriceGet(defaultChannelID, cast.ToInt64(l.ProductVariantID))
			if err != nil {
				return nil, err
			}

			id, err := r.App.DB.CheckoutLineCreate(id, cast.ToInt64(l.ProductVariantID), l.Quantity)
			if err != nil {
				return nil, err
			}

			priceStr := cast.ToString(price)

			item := &model.LineItem{
				ID:               cast.ToString(id),
				Quantity:         l.Quantity,
				ProductVariantID: l.ProductVariantID,
				UnitPrice:        &priceStr,
			}

			lines = append(lines, item)
			commerceLines = append(commerceLines, &commerce.Line{
				ID:               id,
				ProductVariantID: cast.ToInt64(l.ProductVariantID),
				Quantity:         l.Quantity,
				UnitPrice:        price,
			})
		}

		checkout.Lines = lines
	}

	sales, err := r.App.DB.SalesGet()
	if err != nil {
		return nil, err
	}

	priceParams := calculator.PriceParameters{}
	priceParams.Lines = commerceLines

	settings := &calculator.Settings{
		ChannelID: defaultChannelID,
		Sales:     sales,
	}

	calculator := calculator.CalculatePrice(settings, priceParams)

	checkout.TotalPrice = strconv.FormatFloat(float64(calculator.TotalPrice)/100.0, 'f', 2, 64)

	return &model.CheckoutCreate{Checkout: checkout}, nil
}

// CheckoutLinesAdd is the resolver for the checkoutLinesAdd field.
func (r *mutationResolver) CheckoutLinesAdd(ctx context.Context, checkoutID *string, lines []*model.CheckoutLineInput) (*model.CheckoutLinesAdd, error) {

	checkout, err := r.App.DB.CheckoutGetByID(cast.ToInt64(checkoutID))
	if err != nil {
		return nil, err
	}

	newLines := make([]commerce.Line, 0)

	for _, line := range lines {
		newLines = append(newLines, commerce.Line{
			ProductVariantID: cast.ToInt64(line.ProductVariantID),
			Quantity:         line.Quantity,
		})
	}

	err = r.App.DB.CheckoutLinesAdd(checkout.ID, newLines)
	if err != nil {
		return nil, err
	}

	checkout, err = r.App.DB.CheckoutGetByID(cast.ToInt64(checkoutID))
	if err != nil {
		return nil, err
	}

	sales, err := r.App.DB.SalesGet()
	if err != nil {
		return nil, err
	}

	priceParams := calculator.PriceParameters{Lines: checkout.Lines}

	settings := &calculator.Settings{
		ChannelID: defaultChannelID,
		Sales:     sales,
	}

	calculator := calculator.CalculatePrice(settings, priceParams)

	finalLines := make([]*model.LineItem, 0)

	for _, line := range checkout.Lines {
		price := cast.ToString(line.UnitPrice)

		finalLines = append(finalLines, &model.LineItem{
			ID:               cast.ToString(line.ID),
			Quantity:         line.Quantity,
			ProductVariantID: cast.ToString(line.ProductVariantID),
			UnitPrice:        &price,
		})
	}

	return &model.CheckoutLinesAdd{
		Checkout: &model.Checkout{
			ID:         cast.ToString(checkout.ID),
			Lines:      finalLines,
			TotalPrice: strconv.FormatFloat(float64(calculator.TotalPrice)/100.0, 'f', 2, 64),
		},
	}, nil
}

// Hello is the resolver for the hello field.
func (r *queryResolver) Hello(ctx context.Context) (string, error) {
	return "Hello World", nil
}

// Products is the resolver for the products field.
func (r *queryResolver) Products(ctx context.Context, channel string) (*model.ProductList, error) {
	panic(fmt.Errorf("not implemented: Products - products"))
}

// Mutation returns MutationResolver implementation.
func (r *Resolver) Mutation() MutationResolver { return &mutationResolver{r} }

// Query returns QueryResolver implementation.
func (r *Resolver) Query() QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }

// !!! WARNING !!!
// The code below was going to be deleted when updating resolvers. It has been copied here so you have
// one last chance to move it out of harms way if you want. There are two reasons this happens:
//   - When renaming or deleting a resolver the old code will be put in here. You can safely delete
//     it when you're done.
//   - You have helper methods in this file. Move them out to keep these resolver files clean.
const defaultChannelID int64 = 1
