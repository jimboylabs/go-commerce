// Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package model

type Checkout struct {
	ID         string      `json:"id"`
	Email      string      `json:"email"`
	Lines      []*LineItem `json:"lines,omitempty"`
	TotalPrice string      `json:"totalPrice"`
}

type CheckoutCreate struct {
	Checkout *Checkout `json:"checkout,omitempty"`
}

type CheckoutCreateInput struct {
	Channel *string              `json:"channel,omitempty"`
	Email   string               `json:"email"`
	Lines   []*CheckoutLineInput `json:"lines,omitempty"`
}

type CheckoutLineInput struct {
	Quantity         int    `json:"quantity"`
	ProductVariantID string `json:"productVariantID"`
}

type CheckoutLinesAdd struct {
	Checkout *Checkout `json:"checkout,omitempty"`
}

type LineItem struct {
	ID               string  `json:"id"`
	Quantity         int     `json:"quantity"`
	ProductVariantID string  `json:"productVariantID"`
	UnitPrice        *string `json:"unitPrice,omitempty"`
}

type Product struct {
	ID       string            `json:"id"`
	Name     string            `json:"Name"`
	Variants []*ProductVariant `json:"variants,omitempty"`
}

type ProductList struct {
	Edges []*Product `json:"edges,omitempty"`
}

type ProductVariant struct {
	ID       string `json:"id"`
	Sku      string `json:"SKU"`
	Quantity int    `json:"Quantity"`
}
