package calculator

import (
	"math"
	"sort"

	commerce "gitlab.com/jimboylabs/go-commerce/proto"
)

type Settings struct {
	ChannelID int64

	Sales []*Sale
}

func (s *Settings) HasLastItemLowestPriceDiscount() bool {
	for _, s := range s.Sales {
		if s.LastItemLowestPriceDiscount {
			return true
		}
	}

	return false
}

type DiscountLine struct {
	Percentage uint64

	Fixed uint64
}

type LinePrice struct {
	Quantity int

	TotalPrice uint64

	DiscountLines []DiscountLine
}

type Price struct {
	Lines []LinePrice

	TotalPrice uint64
}

type PriceParameters struct {
	Lines []*commerce.Line
}

type SaleChannelListing struct {
	ChannelID     int64
	DiscountValue uint64
}

type Sale struct {
	ID   int64
	Name string

	Type string

	ChannelListing []SaleChannelListing

	EligibleProductVariantIDs []int64

	MinCheckoutItemsQuantity    int
	LastItemLowestPriceDiscount bool

	Active bool
}

func (s *Sale) DiscountPercentage(channelID int64) uint64 {
	if s.Type != "percentage" {
		return 0
	}

	for _, c := range s.ChannelListing {
		if c.ChannelID == channelID {
			return c.DiscountValue
		}
	}

	return 0
}

func (s *Sale) DiscountFixed(channelID int64) uint64 {
	if s.Type != "fixed" {
		return 0
	}

	for _, c := range s.ChannelListing {
		if c.ChannelID == channelID {
			return c.DiscountValue
		}
	}

	return 0

}

func (s *Sale) ValidForProductVariant(productVariantID int64, quantity int, lastItem bool) bool {
	for _, id := range s.EligibleProductVariantIDs {
		if id == productVariantID && quantity >= s.MinCheckoutItemsQuantity && !s.LastItemLowestPriceDiscount {
			return true
		} else if s.LastItemLowestPriceDiscount && id == productVariantID && lastItem && quantity >= s.MinCheckoutItemsQuantity {
			return true
		}
	}

	return false
}

func calculateDiscount(amountToDiscount, percentage, fixed uint64) uint64 {
	var discount uint64
	if percentage > 0 {
		discount = rint(float64(amountToDiscount) * float64(percentage) / 100)
	}
	discount += fixed

	if discount > amountToDiscount {
		return amountToDiscount
	}

	return discount
}

func calculateAmountsForSingleLine(settings *Settings, params PriceParameters, line commerce.Line, multiplier uint64, lastItem bool) LinePrice {
	linePrice := LinePrice{Quantity: line.Quantity}

	unitPrice := line.UnitPrice * multiplier
	linePrice.TotalPrice = unitPrice

	if settings != nil && len(settings.Sales) > 0 {
		for _, sale := range settings.Sales {
			if sale.ValidForProductVariant(line.ProductVariantID, line.Quantity, lastItem) {
				discountLine := DiscountLine{
					Percentage: sale.DiscountPercentage(settings.ChannelID),
					Fixed:      sale.DiscountFixed(settings.ChannelID),
				}
				discount := calculateDiscount(unitPrice, discountLine.Percentage, discountLine.Fixed)

				linePrice.TotalPrice = unitPrice - discount

				linePrice.DiscountLines = append(linePrice.DiscountLines, discountLine)
			}
		}
	}

	return linePrice
}

func CalculatePrice(settings *Settings, params PriceParameters) Price {
	price := Price{}

	if settings != nil && len(settings.Sales) > 0 && settings.HasLastItemLowestPriceDiscount() {
		sort.Slice(params.Lines, func(i, j int) bool {
			return params.Lines[i].UnitPrice > params.Lines[j].UnitPrice
		})
	}

	for i, line := range params.Lines {
		lastItem := i == len(params.Lines)-1

		linePrice := calculateAmountsForSingleLine(settings, params, *line, 1, lastItem)

		price.Lines = append(price.Lines, linePrice)

		itemPriceMultiple := calculateAmountsForSingleLine(settings, params, *line, uint64(line.Quantity), lastItem)

		price.Lines = append(price.Lines, linePrice)

		price.TotalPrice += itemPriceMultiple.TotalPrice
	}

	return price
}

// Nopes - no `round` method in go
// See https://github.com/golang/go/blob/master/src/math/floor.go#L58

func rint(x float64) uint64 {
	return uint64(math.Round(x))
}
