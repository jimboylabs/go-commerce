package calculator

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	commerce "gitlab.com/jimboylabs/go-commerce/proto"
)

func validatePrice(t *testing.T, actual Price, expected Price) {
	assert.Equal(t, expected.TotalPrice, actual.TotalPrice, fmt.Sprintf("Expected nettotal to be %d, got %d", expected.TotalPrice, actual.TotalPrice))
}

func TestNoItems(t *testing.T) {
	params := PriceParameters{nil}

	price := CalculatePrice(nil, params)

	validatePrice(t, price, Price{
		TotalPrice: 0,
	})
}

func TestDiscountSingleItem(t *testing.T) {
	params := PriceParameters{Lines: []*commerce.Line{
		{
			ID:               1,
			ProductVariantID: 1,
			Quantity:         1,
			UnitPrice:        100,
		},
	}}

	settings := &Settings{
		ChannelID: 1,
		Sales: []*Sale{
			{
				Type: "percentage",
				ChannelListing: []SaleChannelListing{
					{
						ChannelID:     1,
						DiscountValue: 10,
					},
				},
				EligibleProductVariantIDs: []int64{1},
				MinCheckoutItemsQuantity:  1,
				Active:                    true,
			},
		},
	}

	price := CalculatePrice(settings, params)

	validatePrice(t, price, Price{
		TotalPrice: 90,
	})
}

func TestDiscountNotValid(t *testing.T) {
	params := PriceParameters{Lines: []*commerce.Line{
		{
			ID:               1,
			ProductVariantID: 1,
			Quantity:         1,
			UnitPrice:        100,
		},
	}}

	settings := &Settings{
		ChannelID: 1,
		Sales: []*Sale{
			{
				Type: "percentage",
				ChannelListing: []SaleChannelListing{
					{
						ChannelID:     1,
						DiscountValue: 10,
					},
				},
				EligibleProductVariantIDs: []int64{2},
				MinCheckoutItemsQuantity:  1,
				Active:                    true,
			},
		},
	}

	price := CalculatePrice(settings, params)

	validatePrice(t, price, Price{
		TotalPrice: 100,
	})
}

func TestDiscountAlexaSpeakers(t *testing.T) {
	params := PriceParameters{Lines: []*commerce.Line{
		{
			ID:               1,
			ProductVariantID: 1,
			Quantity:         3,
			UnitPrice:        10950,
		},
	}}

	settings := &Settings{
		ChannelID: 1,
		Sales: []*Sale{
			{
				Type: "percentage",
				Name: "Buying more than 3 Alexa Speakers will have a 10% Discount on all Alexa speakers",
				ChannelListing: []SaleChannelListing{
					{
						ChannelID:     1,
						DiscountValue: 10,
					},
				},
				EligibleProductVariantIDs: []int64{1},
				MinCheckoutItemsQuantity:  3,
				Active:                    true,
			},
		},
	}

	price := CalculatePrice(settings, params)

	validatePrice(t, price, Price{
		TotalPrice: 29565,
	})
}

func TestLastItemLowestPriceDiscount(t *testing.T) {
	params := PriceParameters{Lines: []*commerce.Line{
		{
			ID:               1,
			ProductVariantID: 2,
			Quantity:         1,
			UnitPrice:        3000,
		},
		{
			ID:               1,
			ProductVariantID: 1,
			Quantity:         1,
			UnitPrice:        539999,
		},
	}}

	settings := &Settings{
		ChannelID: 1,
		Sales: []*Sale{
			{
				Type: "percentage",
				Name: "Each sale of a Macbook Pro comes with a free Raspberry Pi B",
				ChannelListing: []SaleChannelListing{
					{
						ChannelID:     1,
						DiscountValue: 100,
					},
				},
				EligibleProductVariantIDs:   []int64{1, 2},
				MinCheckoutItemsQuantity:    1,
				LastItemLowestPriceDiscount: true,
				Active:                      true,
			},
		},
	}

	price := CalculatePrice(settings, params)

	validatePrice(t, price, Price{
		TotalPrice: 539999,
	})
}

func TestBuyTwoGetOneForFree(t *testing.T) {
	params := PriceParameters{Lines: []*commerce.Line{
		{
			ID:               1,
			ProductVariantID: 1,
			Quantity:         3,
			UnitPrice:        4999,
		},
	}}

	settings := &Settings{
		ChannelID: 1,
		Sales: []*Sale{
			{
				Type: "fixed",
				Name: "Buy 3 Google Homes for the price of 2",
				ChannelListing: []SaleChannelListing{
					{
						ChannelID:     1,
						DiscountValue: 4999,
					},
				},
				EligibleProductVariantIDs: []int64{1},
				MinCheckoutItemsQuantity:  3,
				Active:                    true,
			},
		},
	}

	price := CalculatePrice(settings, params)

	validatePrice(t, price, Price{
		TotalPrice: 9998,
	})
}
