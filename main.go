package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/jimboylabs/go-commerce/cmd"
)

type Channel struct {
	ID           int64
	Name         string
	Slug         string
	CurrencyCode string
}

type Product struct {
	ID          int64
	Name        string
	Description string
}

type ProductVariant struct {
	ID        int64
	ProductID int64
	SKU       string
	Quantity  int
}

type ProductChannelListing struct {
	ID        int64
	ChannelID int64
	ProductID int64
}

type ProductVariantChannelListing struct {
	ID               int64
	ChannelID        int64
	ProductVariantID int64
	PriceAmount      float64
	Currency         string
}

type LineItem struct {
	VariantID int64
	Quantity  int
}

type CheckoutCreateRequest struct {
	ChannelName string
	Email       string
	LineItems   []LineItem
}

var rootCmd = &cobra.Command{
	Use:   "go-commerce",
	Short: "Commerce built with Go",
	RunE: func(cmd *cobra.Command, args []string) error {
		return nil
	},
}

func init() {
	rootCmd.AddCommand(cmd.ServeCmd)
}

func main() {
	defaultChannel := Channel{ID: 1, Name: "default", Slug: "default", CurrencyCode: "IDR"}

	// add a new product
	product1 := Product{ID: 1, Name: "Google Home", Description: ""}
	product1Variant := ProductVariant{ID: 1, ProductID: 1, SKU: "120P90", Quantity: 10}

	// add product to product channel listing
	productChannelListing := ProductChannelListing{ID: 1, ProductID: 1, ChannelID: 1}
	productVariantChannelListing := ProductVariantChannelListing{ID: 1, ProductVariantID: 1, ChannelID: 1, PriceAmount: 49.99, Currency: "IDR"}

	_ = defaultChannel
	_ = product1
	_ = product1Variant
	_ = productChannelListing
	_ = productVariantChannelListing

	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
