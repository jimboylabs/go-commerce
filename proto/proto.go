package proto

type Line struct {
	ID int64

	ProductVariantID int64

	Quantity int

	UnitPrice uint64
}

type Checkout struct {
	ID    int64
	Email string

	Lines []*Line
}
