package cmd

import (
	"net/http"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/charmbracelet/log"
	"github.com/spf13/cobra"
	"gitlab.com/jimboylabs/go-commerce/app"
	"gitlab.com/jimboylabs/go-commerce/graph"
)

const defaultPort = "8080"

var ServeCmd = &cobra.Command{
	Use:     "serve",
	Aliases: []string{"server"},
	Short:   "Start a graphql server",
	Args:    cobra.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		port := os.Getenv("PORT")
		if port == "" {
			port = defaultPort
		}

		cfg := app.DefaultConfig()

		appl, err := app.NewApplication(cfg)
		if err != nil {
			panic(err)
		}

		rs := &graph.Resolver{App: appl}

		srv := handler.NewDefaultServer(graph.NewExecutableSchema(graph.Config{Resolvers: rs}))

		http.Handle("/", playground.Handler("GraphQL playground", "/query"))
		http.Handle("/query", srv)

		// TODO: add graceful shutdown

		log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
		log.Fatal(http.ListenAndServe(":"+port, nil))

		return nil
	},
}

func init() {
	ServeCmd.AddCommand(
		ServeMigrationCmd,
	)
}
