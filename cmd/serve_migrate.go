package cmd

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/jimboylabs/go-commerce/app"
	"gitlab.com/jimboylabs/go-commerce/app/db/sqlite"
	"gitlab.com/jimboylabs/go-commerce/app/db/sqlite/migration"
)

var ServeMigrationCmd = &cobra.Command{
	Use:     "migrate",
	Aliases: []string{"migration"},
	Short:   "Run the server migration tool",
	Args:    cobra.NoArgs,
	RunE: func(cmd *cobra.Command, args []string) error {
		cfg := app.DefaultConfig()
		dp := filepath.Join(cfg.DataDir, "db", sqlite.DbName)
		_, err := os.Stat(dp)
		if err != nil {
			return fmt.Errorf("database does not exist: %s", err)
		}
		db := sqlite.NewDB(dp)
		for _, m := range []migration.Migration{
			migration.Migration0001,
		} {
			log.Print("Running migration", "id", fmt.Sprintf("%04d name %s", m.ID, m.Name))
			err = db.WrapTransaction(func(tx *sql.Tx) error {
				_, err := tx.Exec(m.SQL)
				if err != nil {
					return err
				}
				return nil
			})
			if err != nil {
				break
			}
		}
		return nil
	},
}
