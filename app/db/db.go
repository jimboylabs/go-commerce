package db

import (
	"gitlab.com/jimboylabs/go-commerce/calculator"
	commerce "gitlab.com/jimboylabs/go-commerce/proto"
)

type DB interface {
	CheckoutCreate(channelID int64, email string) (id int64, err error)
	CheckoutLineCreate(checkoutID, productVariantID int64, quantity int) (id int64, err error)
	CheckoutLinesAdd(checkoutID int64, lines []commerce.Line) error
	CheckoutGetByID(checkoutID int64) (commerce.Checkout, error)
	SalesGet() ([]*calculator.Sale, error)
	ProductVariantPriceGet(channelID, productVariantID int64) (price uint64, err error)

	Close() error
}
