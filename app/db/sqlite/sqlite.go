package sqlite

import (
	"context"
	"database/sql"
	"time"

	"github.com/charmbracelet/log"
	"github.com/spf13/cast"
	"gitlab.com/jimboylabs/go-commerce/app/db"
	"gitlab.com/jimboylabs/go-commerce/calculator"
	commerce "gitlab.com/jimboylabs/go-commerce/proto"
	"modernc.org/sqlite"
	_ "modernc.org/sqlite" // sqlite driver
	sqlitelib "modernc.org/sqlite/lib"
)

var _ db.DB = (*DB)(nil)

const (
	DbName = "go_commerce_sqlite.db"

	DbOptions = "?_pragma=busy_timeout(5000)&_pragma=foreign_keys(1)"
)

type DB struct {
	db *sql.DB
}

func NewDB(path string) *DB {
	var err error
	log.Debug("Opening SQLite db", "path", path)
	db, err := sql.Open("sqlite", path+DbOptions)
	if err != nil {
		panic(err)
	}
	d := &DB{db: db}
	err = d.CreateDB()
	if err != nil {
		panic(err)
	}
	return d
}

func (m *DB) CreateDB() error {
	return m.WrapTransaction(func(tx *sql.Tx) error {
		err := m.createChannelTable(tx)
		if err != nil {
			return err
		}

		err = m.createProductTable(tx)
		if err != nil {
			return err
		}

		err = m.createProductVariantTable(tx)
		if err != nil {
			return err
		}

		err = m.createChannelProductTable(tx)
		if err != nil {
			return err
		}

		err = m.createChannelProductVariantTable(tx)
		if err != nil {
			return err
		}

		err = m.createCheckoutTable(tx)
		if err != nil {
			return err
		}

		err = m.createCheckoutLinesTable(tx)
		if err != nil {
			return err
		}

		err = m.createSalesTable(tx)
		if err != nil {
			return err
		}

		err = m.createProductVariantSaleTable(tx)
		if err != nil {
			return err
		}

		err = m.createChannelSaleTable(tx)
		if err != nil {
			return err
		}

		return nil
	})
}

func (m *DB) createChannelTable(tx *sql.Tx) error {
	query := `
	CREATE TABLE IF NOT EXISTS channels(
		id INTEGER NOT NULL PRIMARY KEY,
		name VARCHAR(100) NOT NULL UNIQUE,
		currency VARCHAR(5) NOT NULL
	)`
	_, err := m.db.Exec(query)
	if err != nil {
		return err
	}
	return nil
}

func (m *DB) createProductTable(tx *sql.Tx) error {
	query := `
	CREATE TABLE IF NOT EXISTS products(
		id INTEGER NOT NULL PRIMARY KEY,
		name VARCHAR(50) UNIQUE,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		updated_at TIMESTAMP DEFAULT NULL
	)`
	_, err := m.db.Exec(query)
	if err != nil {
		return err
	}
	return nil
}

func (m *DB) createProductVariantTable(tx *sql.Tx) error {
	query := `
	CREATE TABLE IF NOT EXISTS product_variants(
		id INTEGER NOT NULL PRIMARY KEY,
		product_id INTEGER NOT NULL,
		sku VARCHAR(50) UNIQUE,
		quantity INTEGER NOT NULL DEFAULT 0,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		updated_at TIMESTAMP DEFAULT NULL,
		UNIQUE(id, product_id),

		CONSTRAINT product_id_fk
			FOREIGN KEY (product_id)
			REFERENCES products(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
	)`
	_, err := m.db.Exec(query)
	if err != nil {
		return err
	}
	return nil
}

func (m *DB) createChannelProductTable(tx *sql.Tx) error {
	query := `
	CREATE TABLE IF NOT EXISTS channel_product(
		id INTEGER NOT NULL PRIMARY KEY,
		channel_id INTEGER NOT NULL,
		product_id INTEGER NOT NULL,

		UNIQUE(channel_id, product_id),

		CONSTRAINT channel_id_fk
			FOREIGN KEY (channel_id)
			REFERENCES channels(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
		CONSTRAINT product_id_fk
			FOREIGN KEY (product_id)
			REFERENCES products(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE)`
	_, err := m.db.Exec(query)
	if err != nil {
		return err
	}
	return nil
}

func (m *DB) createChannelProductVariantTable(tx *sql.Tx) error {
	query := `
	CREATE TABLE IF NOT EXISTS channel_product_variant(
		id INTEGER NOT NULL PRIMARY KEY,
		channel_id INTEGER NOT NULL,
		product_variant_id INTEGER NOT NULL,
		currency VARCHAR(5) NOT NULL,
		price_amount VARCHAR(10) NOT NULL,

		UNIQUE(channel_id, product_variant_id),

		CONSTRAINT channel_id_fk
			FOREIGN KEY (channel_id)
			REFERENCES channels(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,

		CONSTRAINT product_variant_id_fk
			FOREIGN KEY (product_variant_id)
			REFERENCES product_variants(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE)`
	_, err := m.db.Exec(query)
	if err != nil {
		return err
	}
	return nil
}

func (m *DB) createCheckoutTable(tx *sql.Tx) error {
	query := `
	CREATE TABLE IF NOT EXISTS checkouts(
		id INTEGER NOT NULL PRIMARY KEY,
		channel_id INTEGER NOT NULL,
		email VARCHAR(100) NOT NULL,
		discount_amount VARCHAR(10) NOT NULL DEFAULT 0,
		total_net_amount VARCHAR(10) NOT NULL DEFAULT 0,

		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		updated_at TIMESTAMP DEFAULT NULL,

		CONSTRAINT channel_id_fk
			FOREIGN KEY (channel_id)
			REFERENCES channels(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
	)`
	_, err := m.db.Exec(query)
	if err != nil {
		return err
	}
	return nil
}

func (m *DB) createCheckoutLinesTable(tx *sql.Tx) error {
	query := `
	CREATE TABLE IF NOT EXISTS checkout_lines(
		id INTEGER NOT NULL PRIMARY KEY,
		checkout_id INTEGER NOT NULL,
		product_variant_id INTEGER NOT NULL,
		quantity INTEGER NOT NULL DEFAULT 1,
		currency VARCHAR(5) NOT NULL,
		total_price_net_amount VARCHAR(10) NOT NULL DEFAULT 0,

		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

		CONSTRAINT checkout_id_fk
			FOREIGN KEY (checkout_id)
			REFERENCES checkouts(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,

		CONSTRAINT product_variant_id_fk
			FOREIGN KEY (product_variant_id)
			REFERENCES product_variants(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
		)`
	_, err := m.db.Exec(query)
	if err != nil {
		return err
	}
	return nil
}

func (m *DB) createSalesTable(tx *sql.Tx) error {
	query := `
	CREATE TABLE IF NOT EXISTS sales(
		id INTEGER NOT NULL PRIMARY KEY,
		name VARCHAR(200) NOT NULL,
		type VARCHAR(20)  NOT NULL DEFAULT percentage,
		min_checkout_items_quantity INTEGER NOT NULL DEFAULT 1,
		last_item_lowest_price_discount INTEGER NOT NULL DEFAULT 0,

		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		updated_at TIMESTAMP DEFAULT NULL
	)`

	_, err := m.db.Exec(query)
	if err != nil {
		return err
	}

	return nil
}

func (m *DB) createProductVariantSaleTable(tx *sql.Tx) error {
	query := `
	CREATE TABLE IF NOT EXISTS product_variant_sale(
		id INTEGER NOT NULL PRIMARY KEY,
		sale_id INTEGER NOT NULL,
		product_variant_id INTEGER NOT NULL,

		UNIQUE(sale_id, product_variant_id),

		CONSTRAINT sale_id_fk
			FOREIGN KEY (sale_id)
			REFERENCES sales(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,

		CONSTRAINT product_variant_id_fk
			FOREIGN KEY (product_variant_id)
			REFERENCES product_variants(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
	)`

	_, err := m.db.Exec(query)
	if err != nil {
		return err
	}

	return nil
}

func (m *DB) createChannelSaleTable(tx *sql.Tx) error {
	query := `
	CREATE TABLE IF NOT EXISTS channel_sale(
		ID INTEGER NOT NULL PRIMARY KEY,
		channel_id INTEGER NOT NULL,
		sale_id INTEGER NOT NULL,
		discount_value INTEGER NOT NULL,

		UNIQUE(channel_id, sale_id),

		CONSTRAINT channel_id_fk
			FOREIGN KEY (channel_id)
			REFERENCES channels(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,

		CONSTRAINT sale_id_fk
			FOREIGN KEY (channel_id)
			REFERENCES channels(id)
			ON DELETE CASCADE
			ON UPDATE CASCADE
	)`

	_, err := m.db.Exec(query)
	if err != nil {
		return err
	}

	return nil
}

func (m *DB) CheckoutCreate(channelID int64, email string) (id int64, err error) {
	query := `INSERT INTO checkouts (channel_id, email) VALUES (?, ?)`
	res, err := m.db.Exec(query, channelID, email)
	if err != nil {
		return 0, err
	}

	return res.LastInsertId()
}

func (m *DB) CheckoutLineCreate(checkoutID, productVariantID int64, quantity int) (id int64, err error) {
	query := `INSERT INTO checkout_lines (checkout_id, product_variant_id, quantity, currency) VALUES (?, ?, ?, ?)`
	res, err := m.db.Exec(query, checkoutID, productVariantID, quantity, "USD") // currency hardcode for now
	if err != nil {
		return 0, nil
	}

	return res.LastInsertId()
}

func (m *DB) CheckoutLinesAdd(checkoutID int64, lines []commerce.Line) error {
	return m.WrapTransaction(func(tx *sql.Tx) error {
		for _, line := range lines {
			query := `INSERT INTO checkout_lines (checkout_id, product_variant_id, quantity, currency) VALUES (?, ?, ?, ?)`
			_, err := m.db.Exec(query, checkoutID, line.ProductVariantID, line.Quantity, "USD")
			if err != nil {
				return err
			}
		}

		return nil
	})
}

func (m *DB) CheckoutGetByID(checkoutID int64) (commerce.Checkout, error) {
	checkout := commerce.Checkout{}
	lines := make([]*commerce.Line, 0)

	err := m.WrapTransaction(func(tx *sql.Tx) error {
		query := `SELECT id, email FROM checkouts WHERE id = ? LIMIT 1`
		r := m.db.QueryRow(query, checkoutID)
		err := r.Scan(&checkout.ID, &checkout.Email)
		if err != nil {
			return err
		}

		query = `
		SELECT 
			cl.id
			, cl.product_variant_id 
			, cl.quantity 
			, cpv.price_amount 
		FROM checkout_lines cl 
		INNER JOIN channel_product_variant cpv ON cl.product_variant_id = cpv.product_variant_id 
		INNER JOIN checkouts c ON cl.checkout_id = c.id 
		WHERE cl.checkout_id = ?
        AND c.channel_id = cpv.channel_id`

		rs, err := m.db.Query(query, checkoutID)
		if err != nil {
			return err
		}
		if rs.Err() != nil {
			return rs.Err()
		}
		defer rs.Close()

		for rs.Next() {
			var l = &commerce.Line{}
			err := rs.Scan(&l.ID, &l.ProductVariantID, &l.Quantity, &l.UnitPrice)
			if err != nil {
				return err
			}

			lines = append(lines, l)
		}

		return nil
	})

	checkout.Lines = lines

	return checkout, err
}

type saleChannelListing struct {
	ChannelID     int64
	DiscountValue uint64
}

func (m *DB) getSaleChannelListing() (map[int64]saleChannelListing, error) {
	var result = make(map[int64]saleChannelListing, 0)

	query := `
	SELECT 	
		channel_id
		, sale_id
		, discount_value
	FROM channel_sale`

	err := m.WrapTransaction(func(tx *sql.Tx) error {
		rs, err := tx.Query(query)
		if err != nil {
			return err
		}
		if rs.Err() != nil {
			return rs.Err()
		}
		defer rs.Close()

		for rs.Next() {
			var channelID, saleID int64
			var discountValue uint64

			err := rs.Scan(&channelID, &saleID, &discountValue)
			if err != nil {
				return err
			}

			_, ok := result[saleID]
			if !ok {
				result[saleID] = saleChannelListing{ChannelID: channelID, DiscountValue: discountValue}
			}
		}

		return nil
	})

	return result, err
}

func (m *DB) getSaleEligibleProductVariant() (map[int64][]int64, error) {
	var result = make(map[int64][]int64, 0)

	query := `SELECT sale_id, product_variant_id FROM product_variant_sale`

	err := m.WrapTransaction(func(tx *sql.Tx) error {
		rs, err := tx.Query(query)
		if err != nil {
			return err
		}
		if rs.Err() != nil {
			return rs.Err()
		}
		defer rs.Close()

		for rs.Next() {
			var saleID, productVariantID int64

			err := rs.Scan(&saleID, &productVariantID)
			if err != nil {
				return err
			}

			v, ok := result[saleID]
			if !ok {
				result[saleID] = []int64{productVariantID}
			} else {
				result[saleID] = append(v, productVariantID)
			}
		}

		return nil
	})

	return result, err
}

func (m *DB) SalesGet() ([]*calculator.Sale, error) {
	var (
		sales []*calculator.Sale
	)

	sc, err := m.getSaleChannelListing()
	if err != nil {
		return sales, err
	}

	spv, err := m.getSaleEligibleProductVariant()
	if err != nil {
		return sales, err
	}

	query := `
	SELECT 
		id
		, name
		, type
		, min_checkout_items_quantity
		, last_item_lowest_price_discount 
	FROM sales`

	err = m.WrapTransaction(func(tx *sql.Tx) error {
		rs, err := tx.Query(query)
		if err != nil {
			return err
		}
		if rs.Err() != nil {
			return rs.Err()
		}
		defer rs.Close()

		for rs.Next() {
			s := &calculator.Sale{}
			err := rs.Scan(&s.ID, &s.Name, &s.Type, &s.MinCheckoutItemsQuantity, &s.LastItemLowestPriceDiscount)
			if err != nil {
				return err
			}

			channelList, ok := sc[s.ID]
			if ok {
				s.ChannelListing = []calculator.SaleChannelListing{{ChannelID: channelList.ChannelID, DiscountValue: channelList.DiscountValue}}
			}

			eligibleProductVariantIDs, ok := spv[s.ID]
			if ok {
				s.EligibleProductVariantIDs = eligibleProductVariantIDs
			}

			sales = append(sales, s)
		}

		return nil
	})

	return sales, err
}

func (m *DB) getProductVariantPrice(channelID, productVariantID int64) (price uint64, err error) {
	var priceAmount string

	query := `SELECT price_amount FROM channel_product_variant WHERE channel_id = ? AND id = ? LIMIT 1`
	r := m.db.QueryRow(query, channelID, productVariantID)
	err = r.Scan(&priceAmount)
	if err != nil {
		return 0, err
	}

	return cast.ToUint64(priceAmount), nil
}

func (m *DB) ProductVariantPriceGet(channelID, productVariantID int64) (price uint64, err error) {
	return m.getProductVariantPrice(channelID, productVariantID)
}

func (m *DB) Close() error {
	log.Debug("Closing db")
	return m.db.Close()
}

func (m *DB) WrapTransaction(f func(tx *sql.Tx) error) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	tx, err := m.db.BeginTx(ctx, nil)
	if err != nil {
		log.Error("error starting transaction", "error", err)
		return err
	}
	for {
		err = f(tx)
		if err != nil {
			serr, ok := err.(*sqlite.Error)
			if ok && serr.Code() == sqlitelib.SQLITE_BUSY {
				continue
			}
			log.Error("error in transaction", "error", err)
			return err
		}
		err = tx.Commit()
		if err != nil {
			log.Error("error committing transaction", "err", err)
			return err
		}
		break
	}
	return nil
}
