package migration

var Migration0001 = Migration{
	ID:   1,
	Name: "sample data seeding",
	SQL: `
	INSERT INTO channels (name, currency) VALUES('default', 'USD') ON CONFLICT (name) DO NOTHING;

	INSERT INTO products (name) VALUES('Google Home') ON CONFLICT (name) DO UPDATE SET updated_at = datetime('now');
	INSERT INTO products (name) VALUES('Macbook Pro') ON CONFLICT (name) DO UPDATE SET updated_at = datetime('now');
	INSERT INTO products (name) VALUES('Alexa Speeaker') ON CONFLICT (name) DO UPDATE SET updated_at = datetime('now');
	INSERT INTO products (name) VALUES('Raspberry Pi B') ON CONFLICT (name) DO UPDATE SET updated_at = datetime('now');

	INSERT INTO product_variants (product_id, sku, quantity) VALUES(1, '120P90', 10) ON CONFLICT (sku) DO UPDATE SET updated_at = datetime('now');
	INSERT INTO product_variants (product_id, sku, quantity) VALUES(2, '43N23P', 5) ON CONFLICT (sku) DO UPDATE SET updated_at = datetime('now');
	INSERT INTO product_variants (product_id, sku, quantity) VALUES(3, 'A304SD', 10) ON CONFLICT (sku) DO UPDATE SET updated_at = datetime('now');
	INSERT INTO product_variants (product_id, sku, quantity) VALUES(4, '234234', 2) ON CONFLICT (sku) DO UPDATE SET updated_at = datetime('now');

	-- publish products into the default channel

	INSERT INTO channel_product (channel_id, product_id) VALUES (1, 1) ON CONFLICT DO NOTHING;
	INSERT INTO channel_product (channel_id, product_id) VALUES (1, 2) ON CONFLICT DO NOTHING;
	INSERT INTO channel_product (channel_id, product_id) VALUES (1, 3) ON CONFLICT DO NOTHING;
	INSERT INTO channel_product (channel_id, product_id) VALUES (1, 4) ON CONFLICT DO NOTHING;

	INSERT INTO channel_product_variant (channel_id, product_variant_id, currency, price_amount) VALUES (1, 1, 'USD', '4999') ON CONFLICT DO NOTHING;
	INSERT INTO channel_product_variant (channel_id, product_variant_id, currency, price_amount) VALUES (1, 2, 'USD', '539999') ON CONFLICT DO NOTHING;
	INSERT INTO channel_product_variant (channel_id, product_variant_id, currency, price_amount) VALUES (1, 3, 'USD', '10950') ON CONFLICT DO NOTHING;
	INSERT INTO channel_product_variant (channel_id, product_variant_id, currency, price_amount) VALUES (1, 4, 'USD', '3000') ON CONFLICT DO NOTHING;

	-- insert sales data
	INSERT INTO sales (id, name, min_checkout_items_quantity) VALUES (1, 'Buying more than 3 Alexa Speakers will have a 10% Discount on all Alexa speakers', 3) ON CONFLICT DO NOTHING;
	INSERT INTO channel_sale(channel_id, sale_id,  discount_value) VALUES (1, 1, 10) ON CONFLICT DO NOTHING;
	INSERT INTO product_variant_sale(sale_id, product_variant_id) VALUES (1, 3) ON CONFLICT DO NOTHING;

	INSERT INTO sales (id, name, min_checkout_items_quantity, last_item_lowest_price_discount) VALUES (2, 'Each sale of a Macbook Pro comes with a free Raspberry Pi B', 1, 1) ON CONFLICT DO NOTHING;
	INSERT INTO channel_sale(channel_id, sale_id,  discount_value) VALUES (1, 2, 100) ON CONFLICT DO NOTHING;
	INSERT INTO product_variant_sale(sale_id, product_variant_id) VALUES (2, 2) ON CONFLICT DO NOTHING;
	INSERT INTO product_variant_sale(sale_id, product_variant_id) VALUES (2, 4) ON CONFLICT DO NOTHING;

	INSERT INTO sales (id, name, type, min_checkout_items_quantity) VALUES (3, 'Buy 3 Google Homes for the price of 2', 'fixed', 3) ON CONFLICT DO NOTHING;
	INSERT INTO channel_sale(channel_id, sale_id,  discount_value) VALUES (1, 3, 4999) ON CONFLICT DO NOTHING;
	INSERT INTO product_variant_sale(sale_id, product_variant_id) VALUES (3, 1) ON CONFLICT DO NOTHING;
`,
}
