package migration

type Migration struct {
	ID   int
	Name string
	SQL  string
}
