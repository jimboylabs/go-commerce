package storage

import (
	"io/fs"
	"os"
)

func EnsureDir(path string, mode fs.FileMode) error {
	_, err := os.Stat(path)
	dp := addExecPermsForMkDir(mode.Perm())
	if os.IsNotExist(err) {
		return os.MkdirAll(path, dp)
	}
	return err
}

func addExecPermsForMkDir(mode fs.FileMode) fs.FileMode {
	if mode.IsDir() {
		return mode
	}
	op := mode.Perm()
	if op&0400 == 0400 {
		op = op | 0100
	}
	if op&0040 == 0040 {
		op = op | 0010
	}
	if op&0004 == 0004 {
		op = op | 0001
	}
	return mode | op | fs.ModeDir
}
