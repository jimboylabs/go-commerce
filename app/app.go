package app

import (
	"path/filepath"

	"github.com/caarlos0/env/v8"
	"github.com/charmbracelet/log"
	"gitlab.com/jimboylabs/go-commerce/app/db"
	"gitlab.com/jimboylabs/go-commerce/app/db/sqlite"
	"gitlab.com/jimboylabs/go-commerce/app/storage"
)

type Config struct {
	DataDir string `env:"GO_COMMERCE_DATA_DIR" envDefault:"data"`
}

func DefaultConfig() *Config {
	cfg := &Config{}
	if err := env.Parse(cfg); err != nil {
		log.Fatal("could not read environment", "err", err)
	}

	return cfg
}

type Application struct {
	Config *Config
	DB     db.DB
}

func NewApplication(cfg *Config) (*Application, error) {
	s := &Application{Config: cfg}
	s.init()

	return s, nil
}

func (a *Application) init() {
	if a.DB == nil {
		dp := filepath.Join(a.Config.DataDir, "db")
		err := storage.EnsureDir(dp, 0700)
		if err != nil {
			log.Fatal("could not init sqlite path", "err", err)
		}
		db := sqlite.NewDB(filepath.Join(dp, sqlite.DbName))
		a.WithDB(db)
	}
}

func (a *Application) WithDB(db db.DB) *Application {
	a.DB = db
	return a
}
