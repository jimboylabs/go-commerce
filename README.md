# Go Commerce

## TODO

- [x] Checkout Create 
- [ ] Checkout update line 
- [ ] Checkout delete line 

## Installation

You will need to have the following installed, before proceeding

- Docker
- Go

### Before you start 

Run database data seed

```
go run main.go serve migrate
```

Run Graphql Server Playground

```
go run main.go serve
```

You should be able to open the playground in `http://localhost:8080`

## Explanation

### Channel

Channel used to configure several aspects such as :

- Product
- Product Variant
- Sale

This enable user to manage product with different currency or regions.

User need at least 1 channel to display products.

Currently the default channel is being hardcoded for easier implementation.

### Product & Product Variant

Each product must have at least 1 Product Variant, since the price will be defined in the product variant level.

Product & Product variant also should be published through channel via `channel_product` & `channel_product_variant` table.

### Cart & Checkout

Go Commerce has no distinct object type for shopping cart & checkout. I wanted the same features like sale to  be available in the cart and the checkout, so it decided to use the same object type for both.

### Sale

There are 2 type of sale:

- Percentage
- Fixed

Eligible product variant should be added to `product_variant_sale` table to define the eligible products which the sales will applied to.

Discount value must be defined as well  in `channel_sale` table, this enable user able to put different value for different channel in the future, but for now it's still hardcoded to only the default channel.

## Mutation Examples

Request below using current database state

```
go_commerce_sqlite.db> select * from channel_product_variant
+----+------------+--------------------+----------+--------------+
| id | channel_id | product_variant_id | currency | price_amount |
+----+------------+--------------------+----------+--------------+
| 1  | 1          | 1                  | USD      | 4999         |
| 2  | 1          | 2                  | USD      | 539999       |
| 3  | 1          | 3                  | USD      | 10950        |
| 4  | 1          | 4                  | USD      | 3000         |
+----+------------+--------------------+----------+--------------+
4 rows in set
Time: 0.006s

go_commerce_sqlite.db> select * from product_variants;
+----+------------+--------+----------+---------------------+---------------------+
| id | product_id | sku    | quantity | created_at          | updated_at          |
+----+------------+--------+----------+---------------------+---------------------+
| 1  | 1          | 120P90 | 10       | 2023-05-09 14:02:10 | 2023-05-13 02:55:46 |
| 2  | 2          | 43N23P | 5        | 2023-05-09 14:02:10 | 2023-05-13 02:55:46 |
| 3  | 3          | A304SD | 10       | 2023-05-09 14:02:10 | 2023-05-13 02:55:46 |
| 4  | 4          | 234234 | 2        | 2023-05-09 14:02:10 | 2023-05-13 02:55:46 |
+----+------------+--------+----------+---------------------+---------------------+
4 rows in set
```

Each sale of a MacBook Pro comes with a free Raspberry Pi B

```
mutation {
  checkoutCreate(
    input: {email: "jimmyeatcrab@gmail.com", lines: [{productVariantID: 2, quantity: 1}, {productVariantID: 4, quantity: 1}]}
  ) {
    checkout {
      id
      email
      lines {
        id
        productVariantID
        unitPrice
        quantity
      }
      totalPrice
    }
  }
}
```

Buy 3 Google Homes for the price of 2

```
mutation {
  checkoutCreate(
    input: {email: "jimmyeatcrab@gmail.com", lines: [{productVariantID: 1, quantity: 3}]}
  ) {
    checkout {
      id
      email
      lines {
        id
        productVariantID
        unitPrice
        quantity
      }
      totalPrice
    }
  }
}
```

Buying more than 3 Alexa Speakers will have a 10% discount on all Alexa speakers
```
mutation {
  checkoutCreate(
    input: {email: "jimmyeatcrab@gmail.com", lines: [{productVariantID: 3, quantity: 3}]}
  ) {
    checkout {
      id
      email
      lines {
        id
        productVariantID
        unitPrice
        quantity
      }
      totalPrice
    }
  }
}
```

Create an Empty checkout and then add new line item

```
mutation {
  checkoutCreate(input: {email: "jimmyeatcrab@gmail.com", lines: []})  {
    checkout {
      id
      lines {
        id
        quantity
        productVariantID
        unitPrice
      }
      totalPrice
    }
  }
}
```

Add the new line

```
mutation {
  checkoutLinesAdd(checkoutId: 1, lines: [{productVariantID: 1,  quantity: 3}]) {
    checkout {
      id
      lines {
        id
        productVariantID
        unitPrice
        quantity
      }
      totalPrice
    }
  }
}
```

## Deployment

Setup persistent volume for storing sqlite database

the default database will be stored in relative path from the workdir which is `/data/db/go_commerce_sqlite.db`

for example if the workdir is `/var/opt/go-commerce` the database will be stored in `/var/opt/go-commerce/data/db/go_commerce_sqlite.db`

Demo URL: https://commerce.rollingsayu.xyz

## Screenshots

![Checkout](images/go_commerce_checkout.png)