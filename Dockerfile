FROM golang:1.20.4-alpine as backend

RUN apk update && apk add git

WORKDIR /app

COPY . .

RUN go build .

FROM alpine:3.16 AS final

RUN mkdir -p /var/opt/go-commerce

WORKDIR /var/opt/go-commerce

RUN apk add --no-cache ca-certificates tzdata

COPY --from=backend /app/go-commerce /app

ENTRYPOINT ["/app"]